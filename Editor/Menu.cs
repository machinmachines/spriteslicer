using System.IO;

using UnityEditor;
using UnityEngine;

namespace MachinMachines
{
    namespace SpriteSlicer
    {
        // Asset menu to create texture arrays
        public class Menu
        {
            const string kAssetMenuItemTextureSlicer = "Assets/MachinMachines/SliceSprites";

            [MenuItem(kAssetMenuItemTextureSlicer, validate = true)]
            static bool SpriteSlicerValidation()
            {
                foreach (UnityEngine.Object obj in Selection.objects)
                {
                    string assetPath = AssetDatabase.GetAssetPath(obj);
                    if (!string.IsNullOrEmpty(assetPath))
                    {
                        if (!Path.GetExtension(assetPath).EndsWith(Texture2DArrayImporter.kSourceFileExtension))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }

            [MenuItem(kAssetMenuItemTextureSlicer)]
            static void SpriteSlicer()
            {
                foreach (UnityEngine.Object obj in Selection.objects)
                {
                    Misc.CreateArrayAssetFromTexture(AssetDatabase.GetAssetPath(obj));
                }
            }
        }
    }
}
