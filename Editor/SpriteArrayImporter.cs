using System.IO;

using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace MachinMachines
{
    namespace SpriteSlicer
    {
        // Wrapper around a "big" sprite asset
        // It allows it to be split in multiple Unity sprites while still being easily manageable
        [ScriptedImporter(k_VersionNumber, kFileExtension)]
        public class SpriteArrayImporter : Texture2DArrayImporter
        {
            public new const string kFileExtension = "spritearray";

            public float PixelsPerUnit;

            private string baseAssetName;

            public override void OnImportAsset(AssetImportContext ctx)
            {
                baseAssetName = Path.GetFileNameWithoutExtension(ctx.assetPath);

                base.OnImportAsset(ctx);
            }

            protected override void OnSourceAssetImportSettings(TextureImporter importer)
            {
                PixelsPerUnit = importer.spritePixelsPerUnit;
            }

            protected override void OnImportTexture(int textureSize, TextureFormat textureFormat, int slicesCount)
            {
                // Nothing to do here for now
            }

            protected override void OnImportSlice(AssetImportContext ctx, Texture2D sliceTexture, int sliceIndex)
            {
                Sprite sprite = Sprite.Create(sliceTexture,
                                              new Rect(0.0f, 0.0f, sliceTexture.width, sliceTexture.height),
                                              new Vector2(0.5f, 0.5f),
                                              PixelsPerUnit);
                sprite.name = $"{baseAssetName}_{sliceIndex}";
                ctx.AddObjectToAsset($"{sprite.name}_texture", sliceTexture);
                ctx.AddObjectToAsset(sprite.name, sprite);
                if (sliceIndex == 0)
                {
                    ctx.SetMainObject(sliceTexture);
                }
            }

            protected override void OnImportFinished(AssetImportContext ctx)
            {
                // Nothing to do here for now
            }
        }
    }
}
