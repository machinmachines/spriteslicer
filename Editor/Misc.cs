using System.IO;

using UnityEditor;
using UnityEngine;

namespace MachinMachines
{
    namespace SpriteSlicer
    {
        public class Misc
        {
            // Duplicate the given texture asset from its path,
            // but with our extension so it can be picked up by the custom importer
            public static bool CreateArrayAssetFromTexture(string assetPath)
            {
                if (!string.IsNullOrEmpty(assetPath))
                {
                    if (Path.GetExtension(assetPath).EndsWith(Texture2DArrayImporter.kSourceFileExtension))
                    {
                        string newAssetPath = Path.Combine(Path.GetDirectoryName(assetPath),
                            $"{Path.GetFileNameWithoutExtension(assetPath)}.{Texture2DArrayImporter.kFileExtension}");
                        File.Copy(assetPath, newAssetPath, true);
                        return File.Exists(newAssetPath);
                    }
                }
                return false;
            }

            // Retrieve the texture 2D asset path matching the given texture array asset path
            public static string GetSourceTextureAssetPath(string textureArrayAsset)
            {
                string result = "";
                if (!string.IsNullOrEmpty(textureArrayAsset))
                {
                    result = Path.Combine(Path.GetDirectoryName(textureArrayAsset),
                        $"{Path.GetFileNameWithoutExtension(textureArrayAsset)}.{Texture2DArrayImporter.kSourceFileExtension}");
                }
                return result;
            }
        }
    }
}
