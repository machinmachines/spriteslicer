using System.IO;

using UnityEditor;

namespace MachinMachines
{
    namespace SpriteSlicer
    {
        public class TexturePreprocessor : AssetPostprocessor
        {
            public readonly string kWatchedPath = Path.Combine("Assets", "_HordeDatas", "_Sprites", "PNG 2048_sliced");

            void OnPreprocessTexture()
            {
                if(Path.GetDirectoryName(assetPath).StartsWith(kWatchedPath))
                {
                    // This creates a copy of the asset with the right extension,
                    // thus triggering a later import of it
                    Misc.CreateArrayAssetFromTexture(assetPath);
                }
            }
        }
    }
}
