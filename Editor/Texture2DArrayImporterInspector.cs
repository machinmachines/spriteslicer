using System;
using System.Reflection;

using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace MachinMachines
{
    namespace SpriteSlicer
    {
        // An extension to the regular texture importer inspector so we can add our UI in it
        // Thanks to https://forum.unity.com/threads/extending-instead-of-replacing-built-in-inspectors.407612/
        [CustomEditor(typeof(Texture2DArrayImporter))]
        public class Texture2DArrayImporterInspector : ScriptedImporterEditor
        {
            // Unity's built-in editor
            AssetImporterEditor defaultEditor;
            bool canApply = false;

            public override void OnEnable()
            {
                base.OnEnable();
                // When this inspector is created, also create the built-in texture importer inspector
                TextureImporter sourceImporter = GetSourceImporter();
                if (sourceImporter != null)
                {
                    defaultEditor = Editor.CreateEditor(sourceImporter, Type.GetType("UnityEditor.TextureImporterInspector, UnityEditor")) as AssetImporterEditor;
                    MethodInfo method = defaultEditor.GetType().GetMethod("OnEnable", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (method != null)
                    {
                        method.Invoke(defaultEditor, null);
                    }
                }
            }

            public override void OnDisable()
            {
                base.OnDisable();
                if (defaultEditor != null)
                {
                    // When OnDisable is called, the default editor we created should be destroyed to avoid memory leakage.
                    // Also, make sure to call any required methods like OnDisable
                    MethodInfo method = defaultEditor.GetType().GetMethod("OnDisable", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (method != null)
                    {
                        method.Invoke(defaultEditor, null);
                    }
                    // TODO figure out how to do this
                    //DestroyImmediate(defaultEditor);
                }
            }

            public override void OnInspectorGUI()
            {
                serializedObject.Update();
                if (defaultEditor != null)
                {
                    defaultEditor.serializedObject.Update();
                }

                using (var scope = new EditorGUI.ChangeCheckScope())
                {
                    if (defaultEditor != null)
                    {
                        defaultEditor.OnInspectorGUI();
                    }

                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("Texture2DArrayImporter", EditorStyles.whiteLargeLabel, GUILayout.Height(32.0f));

                    if (scope.changed)
                    {
                        canApply = true;
                        if (defaultEditor != null)
                        {
                            defaultEditor.serializedObject.ApplyModifiedProperties();
                        }
                        serializedObject.ApplyModifiedProperties();
                    }
                }
                ApplyRevertGUI();
            }

            protected override void Apply()
            {
                base.Apply();
                canApply = false;
            }

            protected override bool CanApply()
            {
                return base.CanApply() || canApply;
            }

            private TextureImporter GetSourceImporter()
            {
                Texture2DArrayImporter importer = target as Texture2DArrayImporter;
                if (importer != null)
                {
                    string sourceAssetPath = Misc.GetSourceTextureAssetPath(importer.assetPath);
                    TextureImporter sourceImporter = AssetImporter.GetAtPath(sourceAssetPath) as TextureImporter;
                    return sourceImporter;
                }
                return null;
            }
        }
    }
}
