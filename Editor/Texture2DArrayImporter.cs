using System.IO;

using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace MachinMachines
{
    namespace SpriteSlicer
    {
        // Wrapper around a "big" sprite asset
        // It allows it to be split in multiple Unity sprites while still being easily manageable
        [ScriptedImporter(k_VersionNumber, kFileExtension)]
        public class Texture2DArrayImporter : ScriptedImporter
        {
            public const int k_VersionNumber = 1;
            // Only handling PNG so far
            public const string kSourceFileExtension = "png";

            public const string kFileExtension = "texture2Darray";

            protected int textureSize;
            protected TextureFormat textureFormat;
            protected int compressionQuality;

            private Texture2DArray texture2DArray = null;

            public override void OnImportAsset(AssetImportContext ctx)
            {
                if (!string.IsNullOrEmpty(ctx.assetPath))
                {
                    if (File.Exists(ctx.assetPath))
                    {
                        // Look for the source asset resolution
                        string sourceAssetPath = Misc.GetSourceTextureAssetPath(ctx.assetPath);
                        GUID sourceGUID = AssetDatabase.GUIDFromAssetPath(sourceAssetPath);
                        if (!string.IsNullOrEmpty(sourceAssetPath) && !sourceGUID.Empty())
                        {
                            TextureImporter textureImporter = TextureImporter.GetAtPath(sourceAssetPath) as TextureImporter;
                            if (textureImporter != null)
                            {
                                TextureImporterPlatformSettings settings = textureImporter.GetPlatformTextureSettings("Standalone");
                                textureSize = settings.maxTextureSize;
                                textureFormat = (TextureFormat)settings.format;
                                compressionQuality = settings.compressionQuality;
                                if(settings.format == TextureImporterFormat.Automatic)
                                {
                                    // Force to BC7 if ever we find "Automatic"
                                    textureFormat = TextureFormat.BC7;
                                }
                                // Additional setup for children classes
                                OnSourceAssetImportSettings(textureImporter);
                            }
                        }
                        EditorUtility.DisplayProgressBar("Texture2DArray import", "Reading texture file...", 0.0f);
                        // The only usage to this is to fill the background with alpha by default
                        byte[] alphaRow = new byte[textureSize * 4];
                        System.Array.Fill<byte>(alphaRow, 0);

                        // First perform the reading of the file and find out how many slices we need
                        Hjg.Pngcs.PngReader pngReader = Hjg.Pngcs.FileHelper.CreatePngReader(ctx.assetPath);
                        Hjg.Pngcs.ImageLines content = pngReader.ReadRowsByte();

                        // We consider reading the file is 30% of the task
                        EditorUtility.DisplayProgressBar("Texture2DArray import", "Reading texture file...", 0.3f);

                        int slicesCount = Mathf.CeilToInt((float)content.ImgInfo.SamplesPerRow / (textureSize * 4));
                        OnImportTexture(textureSize, textureFormat, slicesCount);

                        for (int sliceIdx = 0; sliceIdx < slicesCount; ++sliceIdx)
                        {
                            // Fill the slice data
                            byte[] sliceData = new byte[textureSize * textureSize * 4];
                            if (content.sampleType == Hjg.Pngcs.ImageLine.ESampleType.BYTE)
                            {
                                int index = 0;
                                int maxSourceLength = Mathf.Min(content.ScanlinesB[0].Length - (sliceIdx * textureSize * 4), textureSize * 4);
                                while (index < Mathf.Min(content.Nrows, textureSize))
                                {
                                    System.Array.Copy(content.ScanlinesB[content.Nrows - index - 1], sliceIdx * textureSize * 4, sliceData, index * textureSize * 4, maxSourceLength);
                                    // Take into account non-multiple of the texture size and fill it with alpha
                                    if (maxSourceLength < textureSize * 4)
                                    {
                                        System.Array.Copy(alphaRow, 0, sliceData, maxSourceLength, (sliceIdx + 1) * textureSize * 4 - content.ScanlinesB[0].Length);
                                    }
                                    index += 1;
                                }
                                while (index < textureSize)
                                {
                                    System.Array.Copy(alphaRow, 0, sliceData, 0, textureSize);
                                    index += 1;
                                }
                            }
                            EditorUtility.DisplayProgressBar("Texture2DArray import", "Importing texture...", 0.3f + 0.7f * ((float)sliceIdx + 1.0f) / slicesCount);

                            // Get the raw data into a texture, upload it to the GPU
                            // First as raw data as decoded from the source file
                            Texture2D sliceTexture = new Texture2D(textureSize, textureSize, TextureFormat.RGBA32, false);
                            sliceTexture.alphaIsTransparency = true;
                            sliceTexture.LoadRawTextureData(sliceData);
                            sliceTexture.Apply();
                            // Then it gets transcoded to whatever target format we want
                            EditorUtility.CompressTexture(sliceTexture, textureFormat, compressionQuality);
                            sliceTexture.Apply();
                            if (sliceTexture != null)
                            {
                                OnImportSlice(ctx, sliceTexture, sliceIdx);
                            }
                        }
                        EditorUtility.ClearProgressBar();
                        // Setup dependency to the source texture asset
                        ctx.DependsOnArtifact(sourceGUID);
                        OnImportFinished(ctx);
                    }
                }
            }

            protected virtual void OnSourceAssetImportSettings(TextureImporter importer)
            {
                // Nothing to do here for now
            }

            protected virtual void OnImportTexture(int textureSize, TextureFormat textureFormat, int slicesCount)
            {
                texture2DArray = new Texture2DArray(textureSize, textureSize, slicesCount, textureFormat, false, false);
            }

            protected virtual void OnImportSlice(AssetImportContext ctx, Texture2D sliceTexture, int sliceIndex)
            {
                if (texture2DArray != null)
                {
                    // Add it to the texture array
                    Graphics.CopyTexture(sliceTexture, 0, texture2DArray, sliceIndex);
                }
            }

            protected virtual void OnImportFinished(AssetImportContext ctx)
            {
                if (texture2DArray != null)
                {
                    ctx.AddObjectToAsset("array", texture2DArray);
                    ctx.SetMainObject(texture2DArray);
                }
            }
        }
    }
}
